package com.leetcode.JumpGameII;

public class Solution {
	public static void main(String[] args) {
		int[] input = new int[25000];
		for (int i = 0; i < 25000; i++) {
			input[i] = 25000 - i;
		}
		long start = System.currentTimeMillis();
		System.err.println(new Solution().jump(input));
		System.err.println(System.currentTimeMillis() - start);
	}

	public int jump(int[] A) {
		// IMPORTANT: Please reset any member data you declared, as
		// the same Solution instance will be reused for each test case.
		int[] minSteps = new int[A.length];
		for (int i = 0; i < A.length; i++) {
			minSteps[i] = Integer.MAX_VALUE;
		}
		minSteps[0] = 0;
		for (int i = 0; i < A.length; i++) {
			for (int j = 1; j < A[i] && i + j < A.length; j++) {
				minSteps[i + j] = Math.min(minSteps[i] + 1, minSteps[i + j]);
			}
		}
		return minSteps[A.length - 1];
	}
}