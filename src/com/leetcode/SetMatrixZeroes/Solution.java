package com.leetcode.SetMatrixZeroes;

import java.util.HashSet;
import java.util.Set;

public class Solution {
	public void setZeroes1(int[][] matrix) {
		Set<Integer> rowIdx = new HashSet<Integer>();
		Set<Integer> colIdx = new HashSet<Integer>();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				if (matrix[i][j] == 0) {
					rowIdx.add(i);
					colIdx.add(j);
				}
			}
		}
		for (int i : rowIdx) {
			for (int j = 0; j < matrix[0].length; j++) {
				matrix[i][j] = 0;
			}
		}

		for (int j : colIdx) {
			for (int i = 0; i < matrix.length; i++) {
				matrix[i][j] = 0;
			}
		}
	}

	public void setZeroesConstantSpace(int[][] matrix) {
		int[] flags = findFirstZero(matrix);
		if (flags == null)
			return;

		int flagRow = flags[0];
		int flagCol = flags[1];
		collapsRows(flagCol, matrix);
		collapsCols(flagRow, matrix);

		setRows(flagRow, flagCol, matrix);
		setCols(flagRow, flagCol, matrix);

		for (int j = 0; j < matrix[flagRow].length; j++)
			matrix[flagRow][j] = 0;

		for (int i = 0; i < matrix.length; i++)
			matrix[i][flagCol] = 0;
	}

	private int[] findFirstZero(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++)
			for (int j = 0; j < matrix[i].length; j++)
				if (matrix[i][j] == 0)
					return new int[] { i, j };
		return null;
	}

	private void setRows(int rowIdx, int colIdx, int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			if (i == rowIdx)
				continue;
			if (matrix[i][colIdx] == 0) {
				for (int j = 0; j < matrix[0].length; j++) {
					if (j == colIdx)
						continue;
					matrix[i][j] = 0;
				}
			}
		}
	}

	private void setCols(int rowIdx, int colIdx, int[][] matrix) {
		for (int j = 0; j < matrix[0].length; j++) {
			if (j == colIdx)
				continue;
			if (matrix[rowIdx][j] == 0) {
				for (int i = 0; i < matrix.length; i++) {
					if (i == rowIdx)
						continue;
					matrix[i][j] = 0;
				}
			}
		}
	}

	private void collapsRows(int colIndexToStoreResult, int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (j == colIndexToStoreResult)
					continue;
				if (matrix[i][j] == 0) {
					matrix[i][colIndexToStoreResult] = 0;
					break;
				}
			}
		}
	}

	private void collapsCols(int rowIndexToStoreResult, int[][] matrix) {
		for (int j = 0; j < matrix[0].length; j++) {
			for (int i = 0; i < matrix.length; i++) {
				if (i == rowIndexToStoreResult)
					continue;
				if (matrix[i][j] == 0) {
					matrix[rowIndexToStoreResult][j] = 0;
					break;
				}
			}
		}
	}

	public void setZeroes(int[][] matrix) {
		// setZeroes1(matrix);
		setZeroesConstantSpace(matrix);
	}
	
	public static void main(String[] args)
	{
		Solution s = new Solution();
		s.setZeroesConstantSpace(new int[][]{{1,0}});
	}
}
