package com.leetcode.ReverseNodesinkGroup;

/**
 * Definition for singly-linked list. public class ListNode { int val; ListNode
 * next; ListNode(int x) { val = x; next = null; } }
 */
public class Solution {
	public static class ListNode {
		int val;
		ListNode next;

		public ListNode(int x) {
			val = x;
			next = null;
		}
	}

	public static void main(String[] args) {
		ListNode head = new ListNode(1);
		ListNode cur = head;
		for (int i = 2; i <= 2; i++) {
			cur.next = new ListNode(i);
			cur = cur.next;
		}
		printList(head);
		Solution s = new Solution();
		printList(s.reverseKGroup(head, 2));
	}

	private static void printList(ListNode head) {
		StringBuilder sb = new StringBuilder();
		while (head != null) {
			sb.append(head.val).append(",");
			head = head.next;
		}
		System.err.println(sb);
	}

	public ListNode reverseKGroup(ListNode head, int k) {
		// IMPORTANT: Please reset any member data you declared, as
		// the same Solution instance will be reused for each test case.
		if (k == 0 || k == 1)
			return head;
		if (!haveK(head, k))
			return head;
		ListNode pre = null;
		ListNode cur = head;
		ListNode next = cur.next;
		for (int i = 0; i < k; i++) {
			cur.next = pre;
			pre = cur;
			cur = next;
			if (cur != null)
				next = cur.next;
		}
		head.next = reverseKGroup(cur, k);
		return pre;
	}

	public boolean haveK(ListNode head, int k) {
		ListNode cur = head;
		for (int i = 0; i < k; i++) {
			if (cur == null)
				return false;
			cur = cur.next;
		}
		return true;
	}
}