package com.leetcode.GrayCode;

import java.util.ArrayList;

/**
 * The gray code is a binary numeral system where two successive values differ
 * in only one bit. Given a non-negative integer n representing the total number
 * of bits in the code, print the sequence of gray code. A gray code sequence
 * must begin with 0. For example, given n = 2, return [0,1,3,2]. Its gray code
 * sequence is: 00 - 0 01 - 1 11 - 3 10 - 2 Note: For a given n, a gray code
 * sequence is not uniquely defined. For example, [0,2,3,1] is also a valid gray
 * code sequence according to the above definition. For now, the judge is able
 * to judge based on one instance of gray code sequence. Sorry about that.
 * 
 * @author qw
 *
 */
public class Solution {
	public static void main(String[] args) {
		System.err.println(1 << 1);
		System.err.println(new Solution().grayCode(3));
	}

	public ArrayList<Integer> grayCode(int n) {
		// IMPORTANT: Please reset any member data you declared, as
		// the same Solution instance will be reused for each test case.
		if (n == 0) {
			ArrayList<Integer> a = new ArrayList<Integer>();
			a.add(0);
			return a;
		}
		ArrayList<Integer> l = grayCode(n - 1);
		ArrayList<Integer> ret = new ArrayList<Integer>();
		ret.addAll(l);
		int v = 1 << (n - 1);
		for (int i = l.size() - 1; i >= 0; i--) {
			int vSub = l.get(i);
			ret.add(v + vSub);
		}
		return ret;
	}
}