package com.leetcode.WildcardMatching;

/*
 * Implement wildcard pattern matching with support for '?' and '*'.
 '?' Matches any single character.
 '*' Matches any sequence of characters (including the empty sequence).
 The matching should cover the entire input string (not partial).
 The function prototype should be:
 bool isMatch(const char *s, const char *p)
 Some examples:
 isMatch("aa","a") -> false
 isMatch("aa","aa") -> true
 isMatch("aaa","aa") -> false
 isMatch("aa", "*") -> true
 isMatch("aa", "a*") -> true
 isMatch("ab", "?*") -> true
 isMatch("aab", "c*a*b") -> false
 */
public class Solution {
	public static void main(String[] args) {
		Solution s = new Solution();

		System.err.println(s.isMatch("", ""));

		System.err.println(s.isMatch("abefcdgiescdfimde", "ab*cd?i*de"));

		System.err.println(s.isMatch("a", "a*"));

		System.err.println(s.isMatch("bb", "*?bb"));

		System.err.println(s.isMatch("a", "ab*"));

		System.err.println(s.isMatch("b", "?*?"));

		System.err.println(s.isMatch("b", "?"));

		System.err.println(s.isMatch("aa", "a"));

		System.err.println(s.isMatch("mississippi", "m*issi*iss*"));

		System.err.println(s.isMatch("a", ""));

		long begin = System.currentTimeMillis();
		System.err.println(s.isMatch(
				"abbbaaaaaaaabbbabaaabbabbbaabaabbbbaabaabbabaabbabbaabbbaabaabbabaabaabbbbaabbbaabaaababbbbabaaababbaaa",
				"ab**b*bb*ab**ab***b*abaa**b*a*aaa**bba*aa*a*abb*a*a"));
		// ""));
		long end = System.currentTimeMillis();
		System.err.println("timecost: " + (end - begin));
		System.err.println("count: " + count);
	}

	private String trimPattern(String p) {
		while (p.contains("**"))
			p = p.replaceAll("\\*\\*", "*");
		while (p.contains("*?"))
			p = p.replaceAll("\\*\\?", "*");
		while (p.contains("?*"))
			p = p.replaceAll("\\?\\*", "*");
		return p;
	}

	private static int count = 0;

	private boolean recersiveMatch(String s, String p) {
		count++;
		if (s.isEmpty()) {
			if (p.isEmpty() || p.equals("*"))
				return true;
			return false;
		}
		if (p.isEmpty())
			return false;
		// if (p.contains("*")) {
		char l = s.charAt(0);
		char r = p.charAt(0);
		if (r == '?')
			return recersiveMatch(s.substring(1), p.substring(1));
		if (r != '*' && r == l)
			return recersiveMatch(s.substring(1), p.substring(1));
		if (r != '*' && r != l)
			return false;
		if (r == '*') {
			String subP = p.substring(1);
			if (subP.isEmpty())
				return true;
			char r1 = subP.charAt(0);
			for (int i = 0; i < s.length(); i++) {
				char l1 = s.charAt(0);
				if (r1 == '?' || l1 == r1) {
					if (recersiveMatch(s.substring(i), subP))
						return true;
				}
			}
			return false;
		}
		// }
		/*
		 * else if (p.contains("?")) { if (s.length() != p.length()) return
		 * false; for (int i = 0; i < s.length(); i++) { char l = s.charAt(i);
		 * char r = p.charAt(i); if (r == '?') continue; else if (l != r) return
		 * false; } return true; } else { return s.equals(p); }
		 */
		return false;
	}


	private boolean simpleMatch(String s, String p) {
		String[] patterns = p.split("\\*");
		char[] str = s.toCharArray();

		if (!p.contains("*")) {
			if (p.length() != s.length())
				return false;
			int i = findMatch(s.toCharArray(), p.toCharArray(), 0);
			return i != -1;
		}

		if (!p.startsWith("*")) {
			int len = findMatch(str, patterns[0].toCharArray(), 0);
			if (len != patterns[0].length())
				return false;
		}

		if (!p.endsWith("*")) {
			int len = findMatch(str, patterns[patterns.length-1].toCharArray(), str.length-patterns[patterns.length-1].length());
			if (len != str.length)
				return false;
		}

		int cur = 0;
		for (String pattern : patterns) {
			if (pattern.length() == 0)
				continue;

			cur = findMatch(str, pattern.toCharArray(), cur);
			if (cur == -1 || cur > str.length)
				return false;
		}
		return true;
	}

	private boolean match(char[] s, char[] toMatch, int start) {
		int i = start;
		for (int j = 0; j < toMatch.length; j++)
		{
			int si = i + j;
			if (si >= s.length)
				return false;
			if (s[si] == toMatch[j] || toMatch[j] == '?')
				continue;
			else
				return false;
		}
		return true;
	}

	private int findMatch(char[] s, char[] toMatch, int start) {
		if (start < 0)
			return -1;
		if (toMatch.length == 0)
			return start;

		int cur = start;
		while (cur < s.length) {
			if (match(s, toMatch, cur))
				return cur + toMatch.length;
			cur++;
		}
		return -1;
	}

	public boolean isMatch(String s, String p) {
		// IMPORTANT: Please reset any member data you declared, as
		// the same Solution instance will be reused for each test case.
		//p = trimPattern(p);
		return simpleMatch(s, p);
		//return recersiveMatch(s, p);
	}
}
