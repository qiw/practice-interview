package com.leetcode.BestTimeToBuyAndSellStockII;

/**
 * Say you have an array for which the ith element is the price of a given stock
 * on day i. Design an algorithm to find the maximum profit. You may complete as
 * many transactions as you like (ie, buy one and sell one share of the stock
 * multiple times). However, you may not engage in multiple transactions at the
 * same time (ie, you must sell the stock before you buy again).
 * 
 * @author qw
 *
 */
public class Solution {
	public int maxProfit(int[] prices) {
		// IMPORTANT: Please reset any member data you declared, as
		// the same Solution instance will be reused for each test case.
		if (prices == null || prices.length < 2)
			return 0;
		int profit = 0;
		int position = 0;
		int cost = 0;
		for (int day = 0; day < prices.length; day++) {
			int price = prices[day];
			if (day < prices.length - 1) {
				int nextPrice = prices[day + 1];
				boolean buy = nextPrice > price;
				boolean sell = nextPrice < price;
				if (sell && position == 1) {
					profit += (prices[day] - cost);
					position = 0;
					cost = 0;
				}
				if (buy && position == 0) {
					position = 1;
					cost = prices[day];
				}
			} else // last day, if have position, sell it
			{
				if (position == 1)
					profit += (prices[day] - cost);
			}
		}
		return profit;
	}
}