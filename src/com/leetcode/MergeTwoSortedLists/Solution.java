package com.leetcode.MergeTwoSortedLists;

/**
 * Definition for singly-linked list. public class ListNode { int val; ListNode
 * next; ListNode(int x) { val = x; next = null; } }
 */
public class Solution {
	public static class ListNode {
		int val;
		ListNode next;

		public ListNode(int x) {
			val = x;
			next = null;
		}
	}

	private static void printList(ListNode head) {
		StringBuilder sb = new StringBuilder();
		while (head != null) {
			sb.append(head.val).append(",");
			head = head.next;
		}
		System.err.println(sb);
	}

	public static void main(String[] args) {
		ListNode head1 = new ListNode(1);
		ListNode cur = head1;
		for (int i = 2; i <= 1; i++) {
			cur.next = new ListNode(i);
			cur = cur.next;
		}
		ListNode head2 = new ListNode(2);
		cur = head2;
		for (int i = 2; i <= 1; i++) {
			cur.next = new ListNode(i);
			cur = cur.next;
		}
		Solution s = new Solution();
		printList(s.mergeTwoLists(head1, head2));
	}

	public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
		// IMPORTANT: Please reset any member data you declared, as
		// the same Solution instance will be reused for each test case.
		if (l1 == null)
			return l2;
		if (l2 == null)
			return l1;
		ListNode head = null;
		if (l1.val < l2.val) {
			head = l1;
			l1 = l1.next;
		} else {
			head = l2;
			l2 = l2.next;
		}
		ListNode cur = head;
		while (l1 != null || l2 != null) {
			if (l1 == null) {
				cur.next = l2;
				l2 = null;
				return head;
			}
			if (l2 == null) {
				cur.next = l1;
				l1 = null;
				return head;
			}
			if (l1.val < l2.val) {
				cur.next = l1;
				l1 = l1.next;
			} else {
				cur.next = l2;
				l2 = l2.next;
			}
			cur = cur.next;
		}
		return head;
	}
}