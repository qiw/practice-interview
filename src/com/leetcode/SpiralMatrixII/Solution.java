package com.leetcode.SpiralMatrixII;

/**
 * Given an integer n, generate a square matrix filled with elements from 1 to
 * n2 in spiral order. For example, Given n = 3, You should return the following
 * matrix: [ [ 1, 2, 3 ], [ 8, 9, 4 ], [ 7, 6, 5 ] ]
 * 
 * @author qw
 *
 */
public class Solution {
	public static void main(String[] args) {
		System.err.println(new Solution().generateMatrix(1));
	}

	public int[][] generateMatrix(int n) {
		// IMPORTANT: Please reset any member data you declared, as
		// the same Solution instance will be reused for each test case.
		int[][] ret = new int[n][n];
		if (n == 0)
			return ret;
		int direction = 0; // 0 -> x+1, 1-> y+1, 2->x-1, 3->y-1
		int x = 0;
		int y = 0;
		for (int i = 0; i < n * n - 1; i++) {
			ret[y][x] = i + 1;
			int[] next = getNext(x, y, direction, ret);
			x = next[0];
			y = next[1];
			direction = next[2];
		}
		ret[y][x] = n * n;
		return ret;
	}

	private int[] getNext(int x, int y, int direction, int[][] val) {
		if (direction == 0) {
			int nextX = x + 1;
			if (nextX < val.length && val[y][nextX] == 0)
				return new int[] { nextX, y, direction };
			direction = 1;
		}
		if (direction == 1) {
			int nextY = y + 1;
			if (nextY < val.length && val[nextY][x] == 0)
				return new int[] { x, nextY, direction };
			direction = 2;
		}
		if (direction == 2) {
			int nextX = x - 1;
			if (nextX >= 0 && val[y][nextX] == 0)
				return new int[] { nextX, y, direction };
			direction = 3;
		}
		if (direction == 3) {
			int nextY = y - 1;
			if (nextY >= 0 && val[nextY][x] == 0)
				return new int[] { x, nextY, direction };
			direction = 0;
		}
		return getNext(x, y, direction, val);
	}
}