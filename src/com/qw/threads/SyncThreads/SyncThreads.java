package com.qw.threads.SyncThreads;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;


public class SyncThreads {

	private static Random r = new Random();
	private static Semaphore s = new Semaphore(4, true);
	private static CyclicBarrier b = new CyclicBarrier(4);
	public static void main(String[] args) throws InterruptedException
	{
		System.out.println("start...");
		
		ExecutorService e = Executors.newFixedThreadPool(4);
		
		for (int i = 0; i < 4; i++)
		{
			R r = new R(Integer.toString(i));
			e.execute(r);
		}
		
		e.shutdown();
		e.awaitTermination(100, TimeUnit.SECONDS);
		System.out.println("end!");
	}
	
	static class R implements Runnable
	{
		private String _name;
		public R(String name)
		{
			_name = name;
		}
		@Override
		public void run() {
			try {
				for (int i = 0; i < 10; i++) {

					//s.acquire();

					step(i);
					//s.release();
					//s.acquire(4);
					//s.release(4);
					b.await();
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		private void step(int i)
		{
			System.out.println("thread: " + _name + "; step: " + i);
			try {
				Thread.sleep(r.nextInt(10000));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
